import Vue from "vue";
import VueRouter from "vue-router";
//admin
import OC from "./views/admin/oc/Index.vue";
import Project from "./views/admin/project/Index.vue";

// auth
import Auth from "./views/shares/auth/Auth.vue";
import Login from "./views/shares/auth/Login.vue";

//dash
import Dash from "./views/shares/dash/Dash.vue";
import Home from "./views/shares/dash/Home.vue";


import MachineDetail from './views/admin/machine/Index'

import MachineUpdate from './views/admin/machine/Update'


Vue.use(VueRouter);
const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      //HOME
      path: "/",
      component: Dash,
      redirect: "/home",
      children: [{ path: "home", component: Home,   }]
    },
    //adminoc
    {
      path: "/machines/add",
      component: Dash,
      children: [{ path: "/machines/add", component: OC }]
    },
   
    //adminproject
    {
      path: "/location/add",
      component: Dash,
      children: [{ path: "/location/add", component: Project  }]
    },
    //Login
    {
      path: "/login",
      component: Auth,
      children: [{ path: "/login", component: Login}]
    },
    //MachineDetail
    { 
      path: '/machines/:id/detail/', 
      component: Dash,
      children: [
        {
          name: "machine",
          path: "/machines/:id/:start/:end/detail",
          component: MachineDetail 
        }
      ]
    },

    //edit page
    { 
      path: '/machines/:id/edit', 
      name: 'MachineEdit', 
      component: Dash,
      children: [
        {
          path: "/machines/:id/edit",
          component: MachineUpdate 
        }
      ]
    },


  ]
});

export default router;
